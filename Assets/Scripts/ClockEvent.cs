﻿using UnityEngine;
using System.Collections;

public class ClockEvent : MonoBehaviour
{
    [SerializeField]
    private Transform needleTrans;          // 시계 바늘 회전

    [SerializeField]
    private GameObject bar;                 // 막대기

    [SerializeField]
    private float m_fTime = 0.0f;           // 이벤트가 진행되는 시간

    [SerializeField]
    private float m_fBarReduce = 0.0f;      // 매 Update 호출 시 막대기가 줄어드는 크기

    private UISprite m_BarSprite;        // 막대기 색상 바꾸기
    private float m_fElapsedTime = 0.0f;    // 총 흐르는 시간
    private float m_fBarScale = 1.0f;       // 막대기의 원래 크기 값
    private int m_nNeedleRot = 0;           // 시계 바늘이 회전하는 값
    private Color m_CutinHalf = new Color(1.0f, 0.7f, 0.3f);    // 이 색상은 따로 없어서 만들었음

    public void Play(float time)
    {
        m_fTime = time;
        m_fBarReduce = 1.0f / m_fTime;
        m_BarSprite = bar.GetComponent<UISprite>();
        this.gameObject.SetActive(true);
    }

    private void Awake()
    {
        m_fBarReduce =  1.0f / m_fTime;
        m_BarSprite = bar.GetComponent<UISprite>();
    }

    private void Update()
    {
        m_fElapsedTime += Time.deltaTime;
        NeedleRotate();
        BarImageChange();
    }

    private void NeedleRotate()
    {
        m_nNeedleRot -= 1;
        needleTrans.rotation = Quaternion.Euler(0, 0, m_nNeedleRot);
    }

    private void BarImageChange()
    {
        if (m_fBarScale >= 0.0f)
        {
            m_fBarScale -= m_fBarReduce * Time.deltaTime;
            bar.transform.localScale = new Vector3(m_fBarScale, 1.0f, 1.0f);
        }

        if(m_fBarScale <= 0.3f)
        {
            m_BarSprite.color = Color.red;
        }
        else if(m_fBarScale > 0.3f && m_fBarScale <= 0.5f)
        {
            m_BarSprite.color = m_CutinHalf;
        }
    }
}
