﻿using UnityEngine;
using System.Collections;

public class TimingEventSciprt : MonoBehaviour
{
    [SerializeField]
    private GameObject eventobj;

    [SerializeField]
    private float m_fSpeed = 0.0f;

    [SerializeField]
    private string m_strFunc;   // 실행시킬 함수의 이름

    [SerializeField]
    private GameObject doSomething; // 충돌 시 실행시킬 함수의 오브젝트

    [SerializeField]
    private bool isCollid = false;

    private Rigidbody2D rigid;

    private void Start()
    {
        rigid = this.GetComponent<Rigidbody2D>();
    }

    public void Play(float speed, string str)
    {
        rigid = this.GetComponent<Rigidbody2D>();
        isCollid = false;
        m_fSpeed = speed;
        m_strFunc = str;

        this.gameObject.SetActive(true);
    }

    private void Update ()
    {
        MoveSprite();

        if(Input.GetMouseButtonDown(0) && isCollid)
        {
            doSomething.SendMessage(m_strFunc);
        }
    }

    // 이미지가 왼쪽으로 이동하게 한다.
    void MoveSprite()
    {
        rigid.transform.Translate(Vector3.right * m_fSpeed * Time.deltaTime);
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject == eventobj)
        {
            isCollid = true;
        }
    }
}
