﻿using UnityEngine;
using System.Collections;

public class ButtonEventManager : MonoBehaviour
{
    [SerializeField]
    private GameObject buttonObj;

    [SerializeField]
    private float m_fDelayTime = 0.0f;

    [SerializeField]
    private float m_fMinX = 0.0f, m_fMaxX = 0.0f, m_fMinY = 0.0f, m_fMaxY = 0.0f;

    public static int pressBtnCount = 1;

    private float m_fElapsedTime = 0.0f;

    public void Play(float minX, float minY, float maxX, float maxY, float delay)
    {
        m_fMinX = minX;
        m_fMinY = minY;
        m_fMaxX = maxX;
        m_fMaxY = maxY;
        m_fDelayTime = delay;

        this.gameObject.SetActive(true);
    }

    // 버튼을 누르면 카운트가 감소
    public void DisCountBtn()
    {
        buttonObj.SetActive(false);

        if (pressBtnCount >= 0)
        {
            pressBtnCount--;
            Debug.Log(pressBtnCount);
        }
    }

    // 버튼 이벤트 초기화
    public void PressBtnCountInit()
    {
        pressBtnCount = 3;
    }

    private void Update()
    {
        if (pressBtnCount <= 0)
            return;

        m_fElapsedTime += Time.deltaTime;

        if(m_fDelayTime < m_fElapsedTime)
        {
            m_fElapsedTime = 0;
            

            if (!buttonObj.activeSelf)
            {
                float posX = Random.Range(m_fMinX, m_fMaxX);
                float posY = Random.Range(m_fMinY, m_fMaxY);
                Debug.Log(posX);
                Debug.Log(posY);
                buttonObj.SetActive(true);
                buttonObj.transform.position = new Vector3(posX, posY, buttonObj.transform.position.z);
            }
        }
    }
}
