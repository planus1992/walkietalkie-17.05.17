﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkullButtonScript : MonoBehaviour
{
    [SerializeField]
    private Sprite[] animSpr;     // 애니메이션을 돌리기 위한 스프라이트

    [SerializeField]
    private Sprite pressSpr;      // 버튼을 클릭했을 때 버튼이 눌려진 이미지

    [SerializeField]
    private List<EventDelegate> onClick = new List<EventDelegate>();  // 버튼을 눌렀을 때 행동하는 이벤트

    [SerializeField]
    private bool isPressed = false;

    [SerializeField]
    private int frameIndex = 0;

    [SerializeField]
    private UI2DSprite mNguiSprite;

    [SerializeField]
    private float fFrameTime = 0.0f;

    private float fElapsedTime = 0.0f;

    static public SkullButtonScript current;

	void Update ()
    {
        if (animSpr == null || animSpr.Length <= 0)
            return;

        if (animSpr.Length <= frameIndex)
        {
            frameIndex = 0;
        }

        if (!isPressed)
        {
            fElapsedTime += Time.deltaTime;

            if(fElapsedTime > fFrameTime)
            {
                fElapsedTime = 0.0f;
                mNguiSprite.nextSprite = animSpr[frameIndex];
                frameIndex++;
            }
        }
        else
        {
            mNguiSprite.nextSprite = pressSpr;
        }
	}

    public void OnClick()
    {
        if(current == null && enabled && UICamera.currentTouchID != -2 && UICamera.currentTouchID != -3)
        {
            Debug.Log("Is In Here?");
            current = this;
            EventDelegate.Execute(onClick);
            current = null;
        }
    }
}
