﻿using UnityEngine;
using System.Collections;

public class ButtonAnimation : MonoBehaviour
{
    [SerializeField]
    private UIButton m_button;

    [SerializeField]
    private string[] m_SprName;

    private float m_fElapsedTime = 0.0f;

    [SerializeField]
    private float m_fFrameTime = 0.2f;

    [SerializeField]
    private int m_nIndex = 0;

    private void Start()
    {
        m_button = this.GetComponent<UIButton>();   
    }

    void Update ()
    {
        m_fElapsedTime += Time.deltaTime;

        if(m_fFrameTime < m_fElapsedTime)
        {
            m_fElapsedTime = 0.0f;

            if (m_nIndex < 0 || m_nIndex >= m_SprName.Length)
                m_nIndex = 0;

            m_button.normalSprite = m_SprName[m_nIndex];
            m_nIndex++;
        }
	}
}
