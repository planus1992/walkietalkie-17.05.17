﻿using UnityEngine;
using System.Collections;

public class Animation2D : MonoBehaviour
{
    [SerializeField]
    private int frameIndex = -1;     // 현재 프레임 번호

    [SerializeField]
    private float totalTime = 0.0f;      // 애니메이션이 진행되는 총 시간

    private float frametime = 0.0f;     // 한 애니메이션이 넘어가는 시간
    private float elapsedTime = 0.0f;   //흐르는 시간

    [SerializeField]
    private float waitTime = 0.0f;      // 대기 시간

    [SerializeField]
    private Sprite[] frames;        // 애니메이션 프레임

    [SerializeField]
    private bool loop = false;

    private bool isPlay = true;

    UI2DSprite mNguiSprite;

    // 현재 애니메이션이 진행되나요?
    public bool isPlaying
    {
        get
        {
            return this.enabled;
        }
    }

    public void Pause()
    {
        this.enabled = false;
    }

    public void ResetToBeginning()
    {
        frameIndex = 0;
        this.enabled = true;
    }

    public void Play(int f, int t, int wait)
    {
        frameIndex = f;     // 시작 프레임 인덱스 
        totalTime = t;      // 애니메이션이 끝나는데 걸리는 총 시간
        waitTime = wait;    // 애니메이션이 끝난 후 대기하는 시간
        frametime = totalTime / frames.Length;  // 프레임이 넘어가는데 걸리는 시간
        this.gameObject.SetActive(true);           // 오브젝트 활성화
    }

    private void Awake()
    {
        frametime = totalTime / frames.Length;
    }

    private void Update()
    {
        if (isPlay)
            StartCoroutine(UpdateFrame());
    }

    IEnumerator UpdateFrame()
    {
        // 프레임이 없으면 오브젝트를 꺼버린다.
        if (frames == null || frames.Length == 0)
            this.enabled = false;

        // 초당 프레임이 넘어가는 시간이 있으면
        else if (frametime != 0)
        {
            elapsedTime += Time.deltaTime;

            if (frametime <= elapsedTime)
            {
                elapsedTime = 0.0f;
                if (!loop && (frameIndex < 0 || frameIndex >= frames.Length))
                {
                    this.enabled = false;
                    yield return null;
                }
                if (frameIndex >= frames.Length - 1)
                {
                    StartCoroutine(WaitTime());
                    
                }
                else
                    frameIndex++;

                UpdateSprite();
            }
        }

        yield return null;
    }

    private void UpdateSprite()
    {
        if(!mNguiSprite)
        {
            mNguiSprite = this.GetComponent<UI2DSprite>();

            if(!mNguiSprite)
            {
                this.enabled = false;
                return;
            }
        }
        if (mNguiSprite != null)
        {
            mNguiSprite.nextSprite = frames[frameIndex];
        }
    }

    IEnumerator WaitTime()
    {
        isPlay = false;
        yield return new WaitForSeconds(waitTime);
        frameIndex = 0;
        isPlay = true;
    }
}
