﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneMove : MonoBehaviour
{
    public void onMoveClockScene()
    {
        SceneManager.LoadScene(0);
        Debug.Log("Click");
    }

    public void onMoveTimingScene()
    {
        SceneManager.LoadScene(1);
    }

    public void onMoveButtonScene()
    {
        SceneManager.LoadScene(2);
    }

    public void onMoveDragScene()
    {
        SceneManager.LoadScene(3);
    }
}
