﻿using UnityEngine;
using System.Collections;

public class DragEvent : MonoBehaviour
{
    enum DIRECTIONTYPE
    {
        LEFT = 1,
        LEFTUP,
        LEFTDOWN,
        UP,
        DOWN,
        RIGHT,
        RIGHTUP,
        RIGHTDOWN,
    };

    [SerializeField]
    private Vector3 m_RotVec;

    [SerializeField]
    private DIRECTIONTYPE nType = 0;              // 8방향 중 어느 쪽을 선택하실건가요?

    [SerializeField]
    private float fColRange = 0.0f;               // 드래그 할 때 최소로 인정되는 길이값

    private Vector3 startVec, endVec;             // 마우스 입력 시 좌표 포인터 

    private bool isEventProcessing = false;       // 이벤트 실행 감지

    private bool eventResult = false;                 //이벤트 성공, 실패

    public bool EventAlarm
    {
        get { return eventResult; }
    }

    private void Awake()
    {
        this.transform.rotation = Quaternion.Euler(m_RotVec);
    }

	void Update ()
    {
        KeyProcess();

        eventResult = EventProcess();
    }

    bool EventProcess()
    {
        bool bRet = false;

        if (isEventProcessing)
        {
            // 이벤트를 유저가 처리하면 각도를 검사합니다.
            double length = System.Math.Sqrt(System.Math.Pow(endVec.x - startVec.x, 2) + System.Math.Pow(endVec.y - startVec.y, 2));

            if (length < fColRange) //최소 드래그 해야되는 길이만큼 안했으면
                return false;

            double arc = System.Math.Round(System.Math.Atan2(endVec.y - startVec.y, endVec.x - startVec.x) * 180 / System.Math.PI);

            if (arc < 0)
                arc += 360;

            Debug.Log(arc);

            switch (nType)
            {
                case DIRECTIONTYPE.LEFT:
                    if ((157 <= arc) && (202 > arc))
                        bRet = true;
                    break;
                case DIRECTIONTYPE.LEFTUP:
                    if ((112 <= arc) && (157 > arc))
                        bRet = true;
                    break;
                case DIRECTIONTYPE.LEFTDOWN:
                    if ((202 <= arc) && (247 > arc))
                        bRet = true;
                    break;
                case DIRECTIONTYPE.UP:
                    if ((67 <= arc) && (112 > arc))
                        bRet = true;
                    break;
                case DIRECTIONTYPE.DOWN:
                    if ((247 <= arc) && (292 > arc))
                        bRet = true;
                    break;
                case DIRECTIONTYPE.RIGHT:
                    if ((0 <= arc) && (22 > arc) || ((337 <= arc) && (360 > arc)))
                        bRet = true;
                    break;
                case DIRECTIONTYPE.RIGHTUP:
                    if ((22 <= arc) && (67 > arc))
                        bRet = true;
                    break;
                case DIRECTIONTYPE.RIGHTDOWN:
                    if ((292 <= arc) && (337 > arc))
                        bRet = true;
                    break;
                default:
                    bRet = false;
                    break;
            }
        }
        return bRet;
    }

    void KeyProcess()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startVec = Input.mousePosition;
            isEventProcessing = false;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            endVec = Input.mousePosition;
           
            isEventProcessing = true;
        }
    }
}
